---
title: "Resume"
date: 2020-11-21T15:10:45-05:00
type: "resume"
---

## Summary
---

I am a software engineer that isn't afraid to get his hands dirty, as dirty as software can get anyways.
The lower you go in the software stack, the better.

> Do it right, do it light; do it wrong, do it long

I apply this quote to both my life and my work, focusing on quality and always getting things done the right
way.

Weightlifting, gaming, and personal projects occupy most of my time outside of work.

## Experience
---

**June 2016 - Present:** Cybersecurity Software Engineer, Lockheed Martin Corporation
* Developed the Lockheed Martin Hardened Security[^1] product, a security-first virtualization
solution for commercial servers and embedded systems alongside Intel
* Used C99/C11 for the core virtualization product, along with C++11 and Python 3.x throughout the project
* Principal engineer on multicore/multiprocessor algorithms and design, low-level virtual CPU emulation, and memory
management subsystems
* Spearheaded charge for use of Continuous Integration and automated regression testing using GitLab CI/CD pipelines

[^1]: Formerly known as Intel Select Solution for Hardened Security with Lockheed Martin

**Nov 2013 - May 2016:** Intern/CWEP Student, Lockheed Martin Corporation
* Used C++/Java to develop internal tools for verification of hardware fulfillment of requirements
* Created configuration tools using Java for embedded hardware packages
* Researched and assisted in development of secure virtualization solution for embedded applications in C99
  * This grew into the 'Intel Select Solution for Hardened Security with Lockheed Martin'

## Skills
---

### General
* Develop software products in a constrained and low-level environment
* Design and implement multiprocessor algorithms

### Languages
* Proficient in C, C++, and x86-64 assembly
* Familiar with Python, Java, and Bash
* Novice in JavaScript and Rust (I wish I could use Rust more in my professional work)

### Environments
* Proficient in Linux, it is my primary operating system
	* Very familiar with RHEL, CentOS, Ubuntu, and Arch Linux

### Development Methodology
* Familiar with Git
* Familiar with CI/CD strategies
	* Have used GitLab CI/CD extensively to automate offline testing and deployments into test environments

## Personal Projects
---

### [Scifi Tactics [Working Title, On Hiatus]](https://gitlab.com/Dar13/scifi-tactics)

A turn-based tactics game in the vein of Tactics Ogre: Knight of Lodis or Final Fantasy Tactics, set in a near future
or cyberpunk setting.
* Developed in Godot 3.1 on Linux with an MIT license

### [OpenMemDB](https://github.com/Dar13/OpenMemDB)

An in-memory, non-blocking and wait-free SQL database.
* Developed in C++11 using the Tervel collection of wait-free data structures
* Senior design project at UCF

### [Over The Rhine](https://github.com/Dar13/COP4331Project)

A tower defense game capable of multiplayer built for Android.

* Written in Java using LibGDX and Kryonet
* Year-end project for UCF's COP 4331C
