---
title: "A Review: \"Hades\""
date: 2021-01-23T16:55:00-05:00
article: true
category: "Review"
---

I didn't expect to like Hades, truly. I have tried other roguelites and other roguelikes and they have failed to pull me in and keep my attention long enough.
Hades was different, because it was made by SuperGiant Games the same studio that made Bastion, Transistor, and Pyre. Hades was overflowing with the
best aspects of all their prior games, and combined it all into a truly fantastic experience.

<!--more-->

Hades follows Zagreus, son of Hades, in his attempts to escape the Underworld to find his mother with the help of the Olympian pantheon and the manifestation
of darkness, Nix. You start the game after he makes his first attempt, with a simple but powerful sword and limited abilities. Inevitably, you die. Dying is
expected and normal, much like in a Soulsborne game. You will become quite familiar with the "You Died" screen. Even successful runs end with death in some
form.

However, death is not the end of progression. Some collectible resources persist after death and can be used on Zagreus' abilities, decorations around
Hades' house, or upgrading Zagreus' weapons. Upgrading your abilities will be essential to making it through Tartarus and the other zones in the game to
get to the surface.

The combat is well-executed, with a great amount of variety and interplay between run-specific weapon upgrades (provided by the missing Greek smith Daedalus) and
Boons given to Zagreus by the Greek pantheon. Just like most players will settle on certain weapons and configurations to play with in other games
like Call of Duty or Monster Hunter, you figure out that you really like certain combinations of a particular weapon, character abilities and Boons.
Sometimes a monkey wrench is tossed into the mix when you can't get the Boons you want while using a weapon which forces players to abandon their
normal playstyle or else fail the run in later parts of the labyrinth.

As good as the combat is, at the end of the day it is just a vehicle for the narrative to use. Throughout your numerous escape attempts Zagreus will run
into Greek heroes, prisoners, creatures, gods and goddesses that all have unique interactions with Zagreus. From the intensely nervous Dusa to the
equally cocksure champion of Elysium, each character has an entirely unique style that always left me hungry for the next step in their plotline.
These interactions are what kept me playing this game far more than any other roguelike/roguelite. Each interaction was natural and fit the characters
perfectly, such as the nervousness of Zagreus' first interactions with the Greek pantheon and his consistent kindness towards the tortured souls within
the Underworld.

The sheer amount of narrative-based and unique interactions between all the characters would be impressive on its own. But SuperGiant took it a massive
leap forward towards legendary status, as all of these interactions are voice-acted. All of them! There are over 10,000 voice-acted lines in the game,
and I didn't start hearing repeated lines until 10 hours into the game. The voice actors did a phenomenal job and truly brought the game to life,
even the undecipherable utterings of Charon.

To top it off, the game also includes the best implementation of an 'easy mode' that I've ever seen. The game calls it 'God Mode', which reduces the amount
of damage you receive from enemy attacks. It starts at a 20% damage reduction, and if you die with 'God Mode' enabled the game increases the damage reduction
amount by 2%. There's no penalty for enabling 'God Mode' and you can toggle it off or on at any point in a run. If there's a particular boss that always kills
you or takes too much health away (looking at you Bone Hydra) you can turn it on for the boss fight and then disable it afterwards. On a gameplay/mechanical
level, this is my favorite feature and really increased the level of fun I got out of the game.

I greatly enjoyed my time with Hades (100+ hours) and heartily recommend it to fans of the roguelite/roguelike genre. For others, it's a difficult thing
to outright recommend for everyone because the game does have a level of difficulty that can't be avoided. So it's much like recommending Dark Souls or
Bloodborne for me, it really depends on how much challenge you like to have in your video games.

As always, I'd be happy to talk more about Hades or take recommendations of games to play/review via email (found in my resume).
