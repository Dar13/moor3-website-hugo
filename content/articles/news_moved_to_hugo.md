---
title: "News: Moved website to Hugo"
date: 2020-12-24T10:51:23-05:00
article: true
category: "News"
---

While I enjoyed the bare-bones approach of handwriting my personal site with only
HTML/CSS and a bit of handwritten Javascript, the maintainability isn't that great.
Converting articles like my reviews were painful as I'd type them up in LibreOffice
and then bring them into the HTML document as writing directly in HTML isn't
exactly a pleasant experience to me.

In Hugo, I think I'll be able to skip that conversion step altogether as I think
writing in Markdown is significantly easier than in raw HTML, plus I don't have
to worry about remembering to update all the links or worry about external site
integration (bye-bye Write.As!).

Not much else to report, but enjoy the new site! Feel free to open an issue on
the [repository](https://gitlab.com/Dar13/moor3-website-hugo) if you encounter
any issues.
