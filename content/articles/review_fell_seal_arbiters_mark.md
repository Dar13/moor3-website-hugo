---
title: "A Review: \"Fell Seal: Arbiter's Mark\""
date: 2019-08-20T17:06:06-05:00
article: true
category: "Review"
---

I have a deep fondness for tactical role-playing games that comes from my childhood on the Game Boy
Advance and Game Boy Advance SP, where I spent many many hours maximizing character levels and equipment
and outwitting the AI in a battle as quickly as I could. My favorite game from back then was
“Tactics Ogre: Knight of Lodis”, and “Fell Seal: Arbiter’s Mark” strongly reminds me of it in the best
of ways. This is a review of sorts and will contain spoilers in some respects though I’ll try to avoid
large story spoilers.<!--more-->

To start, the game-play in Fell Seal is a callback to the systems present in those older games with each
character having a primary class with specific abilities that the player can moderately customize and
progress how they wish. Fell Seal adds a secondary class to each character which dramatically increases
the amount of skills and spells available to each character. I enjoyed the increase in variety as it
reduced the need for specialized characters within the party and allowed the developers to reduce the
size of the parties taking part in battles. Without that secondary class, it would have significantly
more difficult to adapt to the different enemy party compositions without grinding up additional
situational characters. The amount of classes and types of character are vast, making unique party
compositions and combinations of classes in a character ever more numerous. If you wanted a glass cannon
spellcaster, you can make at least 12 combinations of primary and secondary classes to make it. More
exotic combinations like Templar/Warmage or Gambler/Reaver introduce a great source of variety for both
the player and the AI making each battle interesting and fresh as you do not expect a global top-tier
spell to come from the guy dressed as a barbarian raider! The class system was a breath of fresh air
even though it took some time for the progression and class mastery system to settle in my head.

As far as the actual battles go it was a polished and well-implemented form of the traditional turn-based
mechanics I had grown up with Tactics Ogre which felt immediately natural and comfortable. The choice to
make items only consumable per-battle rather than globally was an great choice, as I was definitely one of
those that would hoard them to the end. Whoever came up with that, I could kiss you! Attacks feel weighty,
spells are magical, and animations are short and interesting which is important when you see them so many
times in a play-through. Some animations felt too long (such as the animation for the teleportation boots
and the teleportation door), but did not take away from the overall experience. One important difference
from Tactics Ogre that I was both happy to see and disappointed in was the decision to have projectiles
ignore terrain that may be in the way (more on that later). The developers also chose to use an intermixed
turn order, though at least you can see who is upcoming and prepare accordingly. The intermixed order
does allow skills such as ‘Haste’ and ‘Quicken’ to exist. These two skills are were quite useful in making
strategies that can turn a battle around in the blink of an eye by either making a character’s turn come
quicker or immediately take a turn, respectively. All of this gives a lot of tools for players to create
interesting tactics or one-two combos that take advantage of good interactions between skills.

However, I do have some critiques regarding the gameplay. There are two mechanics in particular that I
find not fun at best and infuriating at worst, especially when they interact. The first of these is the
aforementioned decision to have projectiles and skills ignore terrain. I can understand why the developers
made this choice as it makes being a bow user and certain skills quite difficult to implement and/or use,
but I disagree with it. Using the terrain to protect your back or sides is an exercise in futility unless
the height differential is quite significant as your opponent can hit you anyways. With how unique and
fun the terrain can be in the maps some tactics are simply not possible. As an example, I wish I could
send a Reaver or Templar on a flanking maneuver on maps with sheer cliffs like Gyaum Tor and have them keep
their back to the cliff to avoid being sniped by Rangers or Gunners on top of the cliff.

The other critique is on how water works in this game. I like how it looks and how characters look while
swimming in it. I don’t like how it functions. I don’t like how getting pushed into it as certain classes
causes instant death unless you have the hover boots on. I especially do not like how there are skills
that cause position swaps, cannot miss, cannot be countered, cannot be blocked, and can be used to throw
characters into the water and thus to their deaths. It feels cheap every time it happens to me in a
game and I end up reloading a save and trying to rush down that single enemy. A particularly nasty
interaction with the mechanic I critiqued above causes even more frustration. During one battle in Kapawka
Jungle, a map with a bridge on one side of the map that crosses a stream, I placed Kyrie on the main
part of the bridge thinking that she would be safe from enemies down in the water that had the ‘Flip Kick’
position-swap ability. Due to skills ignoring terrain and only using height differences, the enemy used
‘Flip Kick’ to swap positions with Kyrie and instantly killed her. The AI is also unfailing in exploiting
this interaction which makes maps with lots of water frustrating and not fun if any of the enemies have
a position-swap or push ability.

Some people would say that's part of the game and I should not have used heavy armor classes on levels
with water in them or counteract that with items. Punishing heavy armor characters or players that want
a tank-like character rubs me the wrong way and either way you cut it, this is a punishment for choosing
to use heavy armor that other classes simply don't have to deal with. Especially when the heavy armor does
not always “feel” like it is worthwhile to use at all.

Equipment in this game has plenty of variety though it can feel like it is not an overly important
component in a character's composition. Increases in character attributes are relatively minor between
weapon and armor tiers except for some special items that have large boosts or are just better than
normal equipment. It was fairly often I would upgrade equipment for meager 3-5 point increases on two
or three attributes. Obtaining special items was more exciting due to real trade-offs between attributes,
and the crafting system helped add more of that excitement as the story progressed. A nitpick I have on
the equipment was that certain weapon or armor descriptions would hint at special effects the items had
but did not actually define or elaborate on them.

An example of this is an intriguing set of cursed armor that hinted that battle sated its thirst. It had
poor elemental resistances and was not all that great compared to the armor I had at that point in the
game. I had no idea what sating the armor’s thirst meant and I don’t have that much time these days to
experiment to find out what it does or if it is just kinda bad armor. It could be that the attribute boosts
were worthwhile to lower-level characters or earlier in the game but I was too over-leveled for it at
that point. Hard to tell.

The story was a good mix of a “who-dunnit” alongside more classic “we need to save the world, go to these
three temples and do stuff” fantasy themes. The relationship between Kyrie and Reiner is much like I would
imagine how I would be if I worked with my brother with all the bad jokes and good cooking. The characters
you meet along the way each have their distinguishing traits and distinct style to set them apart and show
off those traits such as the aloof necromancer, sly bounty hunter, and corrupt noble. The various arcs of
these characters’ personalities and mini-stories did not feel out of place or forced. Anadine’s story in
particular was one I quite liked, going through here journey as she was forced into accepting a corruptive
power and rolled with the punches to grow stronger without becoming resentful of the additional burden nor
the quest she continued.

The character creation in this game is phenomenal with a multitude of customization options for everything
from eye color and shape to specific pieces of clothing from multiple different classes. You want a werewolf
mage? You got it! Want a ninja with a templar armor aesthetic? All yours! My favorite part by far was the
abundance of character portraits available. Each of them are brimming with a certain history and type of
character behind them. Such as a bright-eyed cleric looking ready to keep everyone up and healthy or a raging
ogre warrior ready to tear enemies limb from limb, or anything in between. It truly helps you the player
create a personality and backstory for these characters to explain how they look in those portraits and
influences how you dress them accordingly.

I had a quartet of custom characters that I grew quite accustomed to:

 * Lili, a marksman that carries her cheery disposition into battle and rains chaos from a distance
 * Virgil, a severe-looking wizard that eventually succumbed to the temptations of dark magic
 transforming into a powerful sorcerer lich
 * Lana, a mender that discovered she was a long-lost princess and uncovered a great ability to
 perform holy spells
 * Loche, a knight that grew in prestige and strength into a powerful lord able to command powerful
 abilities to weaken foes and empower allies

All of these were an important part of my party alongside the expected story characters, and I came to
depend on them. I remember feeling a similar connection to some custom characters in Tactic Ogre, but
not to this degree and I credit this to the fantastic character portraits that give a certain personality
and distinctness to each character rather than a bland or featureless stand-in (though the game has those
too for those who don’t care or can’t decide). This is an additional source of immersion and emergent
story-building that I typically find myself creating in role-playing games rather than tactics and strategy
games. Definitely something other developers, myself included, should consider when designing character
creation or customization options.

All in all, this game was a fantastic ride of 55+ hours and I cannot recommend it enough for fans of
tactics games. It’s a worthy addition to the genre alongside Final Fantasy Tactics and Tactics Ogre
and I look forward to 6 Eyes Studio’s next adventure!

Thank you for reading this “review” of “Fell Seal: Arbiter's Mark”! I'm going to be continuing to do
reviews of games I play and feel strongly for in this space. There will be other series or standalone
posts but I do not have any sort of schedule. If there's a topic you would like to hear my thoughts on
or want to discuss this further you can reach me via email (found in my resume).
