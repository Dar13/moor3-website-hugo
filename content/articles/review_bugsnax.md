---
title: "A Review: \"Bugsnax\""
date: 2022-10-02T11:41:22Z
article: true
category: "Review"
---

Games that use a cutesy, simple, and relatively innocent aesthetic but deal with far more mature themes
are an interesting breed of game to me. Balancing the aesthetic and mood against the mature themes is a
fascinating phenomena to witness and seeing the 'cracks' in the facade, whether intentional or not always
brings a 'Ah-ha!' type moment that's very satisfying. Bugsnax rides that line extremely well, the mystery
is omnipresent and interesting. It's a fun little monster catching game in an interesting scenario with
engaging characters and fun puzzles. <!--more-->

I played Bugsnax on my Arch Linux install on my PC, and other than forcing it to run in DX11 mode on first
startup, ran into zero issues. Loading times were a little long, especially on startup, but not obnoxiously
long. Controller support via Steam Input was excellent, and I can only recommend using a controller instead
of keyboard and mouse. The control scheme is very very targeted towards controllers. Overall performance was
great, never dropping below 60 FPS (I use a 4K 60 Hz monitor), no matter what was happening on screen. This
is to be expected considering the graphical style chosen for the game. If performance was worse I'd be very
disappointed in the game developer as that would be developer error, not lacking hardware (as I am using a
AMD Ryzen 5900X, 32 GiB RAM, with AMD Vega 56 GPU).

Enough about the technical side of the game, time for gameplay and the touchy-feely part of a game review. I
really enjoyed my time with Bugsnax and ended up 100%'ing the game, making it one of the few 'perfect' games
on my Steam account. The atmosphere never got annoying, the creatures were interesting and required some
ingenious thinking to catch them efficiently, or catch them at all. It's a relatively short game as well,
which works out great for me as I don't have as much free time as I used to these days.

The story isn't particularly poignant or thought-provoking, but the mystery of the island and the Bugsnaks
was done well. The introduction of the prior residents of the island and the follow-on implications was fun
and never got too serious to cause a real dichotomy with the rest of the game.

All in all, I don't have any strong feelings either way on the game but I did enjoy my time with it. The DLC
is more of the same goodness, and is free which is always nice.

Thank you for reading this short and sweet "review" of "Bugsnax"! Until next time, ciao!
