---
title: "A Review: \"The Riftbreaker\""
date: 2022-02-01T16:55:00-05:00
article: true
category: "Review"
---

When I put "The Riftbreaker" on my Steam wishlist, I thought it would be similar to Exor's prior game, X-Morph Defense.
I am pleased to say that I'm happy it wasn't. "The Riftbreaker" is a great game, that combined the fun
parts of "They Are Billions" with parts of Factorio's base and resource management systems mashed together in a very
satisfying combination.

<!--more-->

As great as the game is, I have to get something out of the way first. The main menu background and introductory
cutscene look quite bad.  The main menu background makes it look like a mobile ad game, complete with the somewhat
animated bobbing of the robot and character against a blurred background. The cutscene looks like an old video card
demo video from the pre-Crysis days. It's disappointing, especially in the age of gorgeous animation like the
Astartes Project (which is fantastic by the way, I highly recommend you check it out!).

Now that we have the biggest disappointment out of the way, on to the good parts!

The combat gameplay is a ton of fun, with very weighty melee and satisfying explosions. The destruction physics
are super fun to play with but aren't terriby realistic, but I'm not playing a game about being a giant mech using
a hammer to smash up-sized spiders that spit acid for ultra-realistic destruction physics. Plus, I do kind of
expect things to shatter and fly when said hammer hits something. The variety of weapons is quite high as well
with everything from the classic sword and hammer to a spear, claws, etc for melee and machine gun, minigun, rocket launcher,
grenade launcher, acid spitter, etc for ranged. I preferred running around with a hammer and minigun combo, but
the rocket launcher was quite fun to use too.

Combat and base management are done in phases as you have to be physically present in order to the vast majority
of tasks. So if one of your resource gathering areas is under attack while you're setting another solar panel
farm, you may have to run or warp to the area under attack unless you really loaded up these areas with turrets.

Base management is OK, it's not amazing or straightforward like it would be in games built entirely around that
concept or in a RTS or whatever but it works. Beginners to the game are going to be unaware of how much space
some of these buildings take up and expanding the base afterwards can be awkward unless you plan ahead for it.
Players that have played a game like 'They Are Billions' will be right at home as similar expansion strategies
will work quite well.

An interesting remix is that you are, to a degree, forced to make smaller and isolated resource gathering bases
that aren't as heavily fortified. These bases aren't impenetrable bulwarks against the larger invasion waves,
instead serving as barrier islands of sorts that delay big waves or deny small  waves which give the player time
to warp around the map and destroy the waves manually. Saves a bunch of AI Cores and rarer resources, which
you'll need for more exotic and fun turrets you'll place in your primary base. It wasn't easy getting over my
desire to heavily fortify every single base to resist the heaviest of waves, but once you're able to do that
the game gets a lot more straightforward as you don't find yourself scrounging for resources nearly as much or
reworking the entire defense grid of the primary base every 5 to 10 minutes.

This game was great, and I loved my time with it. It has great replay value for those who really enjoy games
like "They Are Billions" and Factorio while having gorgeous physics-driven explosions. The developers have
continued adding new environments, weapons, and other content to the game since release which is awesome of them.
I heartily recommend this game to everyone that liked "They Are Billions" and other base-building/tower-defense
games.

Thank you for reading this "review" of "The Riftbreaker"! If you have further recommendations or ideas for
future reviews, feel free to reach out via email (found in my resume).
