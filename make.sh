#!/usr/bin/bash

IMAGE_NAME="registry.gitlab.com/dar13/moor3-website-hugo/build:latest"
OUTPUT_DIR="./build"

if [ "$1" == "debug" ]
then
	OPTS="-D"
fi

rm -rf ${OUTPUT_DIR}

podman run --rm -t -v $(pwd):/home/project:z ${IMAGE_NAME} hugo ${OPTS} -d ${OUTPUT_DIR}
