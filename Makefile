IMAGE_NAME := registry.gitlab.com/dar13/moor3-website-hugo/build:latest
OUTPUT_DIR := ./build

PODMAN_CMD := podman run --rm -t
PODMAN_VOL := -v $(shell pwd):/home/project:z

.PHONY: debug
debug:
	$(PODMAN_CMD) $(PODMAN_VOL) $(IMAGE_NAME) hugo -D -d $(OUTPUT_DIR)

.PHONY: shell
shell:
	$(PODMAN_CMD) -i $(PODMAN_VOL) $(IMAGE_NAME) sh

.PHONY: run
run:
	$(PODMAN_CMD) -i --network host $(PODMAN_VOL) $(IMAGE_NAME) hugo server

.PHONY: run_draft
run_draft:
	$(PODMAN_CMD) -i --network host $(PODMAN_VOL) $(IMAGE_NAME) hugo server -D --noHTTPCache

.PHONY: clean
clean:
	rm -rf $(OUTPUT_DIR)
